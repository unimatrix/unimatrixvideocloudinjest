# MPEG-DASH stream blob storage to Azure VideoIndexer implementation
This project is to do an implementation of a mechanism that consumes what an unimatrix device would send to cloud blob storage if we decide to go with MPEG-Dash stream upload to blob way of pushing video to the cloud for analytics.
# Implementation Details
The implementation consist of having two azure functions in order to bring the MPEG-DASH stream content to Azure Video Indexer service. 
## First function : Convert MPEG-DASH M4S chunks into MP4
Since Azure Video Indexer is unable to process MPEG-DASH chunks, we need first a function that will produce a standard MP4 file by combining the init stream file with a chunk file. With this file combinaison, chunk will become playable as a standard MP4 file. The output of the function will be a MP4 file that is written in the same blob folder as the source M4S file.
## Second function : Upload MP4 file to Azure Media Indexer
The second function will trigger on new chunk file that is an MP4. It will simply take that MP4 and upload it to Azure Media Indexer.
# Launching the application
## Prerequisites
* An Azure Subscription [Create for free *12 months free services. Limitations applies.*)](https://account.azure.com/signup?offer=ms-azr-0044p&appId=102&ref=azureplat-generic&redirectURL=https%3a%2f%2fazure.microsoft.com%2fen-ca%2fget-started%2fwelcome-to-azure%2f&l=en-ca&correlationId=20F78CB73CAF6C2D391782593D576DE0)
* A Video Indexer Subscription [+Create new account](https://www.videoindexer.ai/)
## Using VS Code
This sample application has been done using VS Code. We recommand that you also use VS Code since it will automatically get the proper extensions in order to deploy your application in your subscription.
### Used VS Code extensions
* [Azure Functions](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azurefunctions)
* [Azure Account](https://marketplace.visualstudio.com/items?itemName=ms-vscode.azure-account)
* [C#](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
## Application Configuration
There is a set of configuration that need to be set in order to have a working application. Those configurations are related to storage account where the MPEG-DASH will be stored and necessary informations in order to upload video file to Azure Media Indexer
1. **AzureWebJobsStorage** : Storage account connection string that will be used for two things: Be the source of record of this Azure Function Application and Storage for files that will be sent by the unimatrix device. To avoid consuming Azure Ressources when you develop and debug a local instance, you can set the value to "UseDevelopmentStorage=true". This will use the [Azure Storage Emulator](https://docs.microsoft.com/en-us/azure/storage/common/storage-use-emulator)
2. **VideoIndexerApiUrl** : The url of your Azure Video Indexer Application. If you use the trial version, the url is "https://api.videoindexer.ai".
3. **VideoIndexerLocation** : The location of your Azure Video Indexer Application. If you use the trial version, the location is <"trial">. If you use your own instance. it will be the datacenter location.
4. **VideoIndexerAccountId** : Your own Video Indexer Account ID. This is an accout that you can obtain on the [VideoIndexer](https://www.videoindexer.ai/) website. Retrieving the account ID is a little bit tricky. The way I managed to find it is by uploading a small video clip through the Video Indexer portal and clicking on it when the video is completed. The URL of the video analysus page shows .../accounts/{VideoIndexerAccountId}/videos/...
5. **VideoIndexerApiKey** : The api key in used to obtain access tokens that give permission to do Video Indexer API calls. This key can be fetched in your user settings on the Video Indexer portal. Click on your user icon on the top right of the webpage. Then, click on "Settings" menu item. On the "Settings" page, click on the "Account" tab. You will see in that page the AccountID section which contains your personnal Account ID.
## Local Deployment
### Configuration
When you develop, the configuration will be fetched from your ("local.settings.json")[https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=windows%2Ccsharp%2Cbash#local-settings-file] file.  
```json
{
  "IsEncrypted": false,
  "Values": {
    "AzureWebJobsStorage": "UseDevelopmentStorage=true",
    "FUNCTIONS_WORKER_RUNTIME": "dotnet",
    "VideoIndexerApiUrl": "https://api.videoindexer.ai",
    "VideoIndexerLocation": "trial",
    "VideoIndexerAccountId": "1e543be7-be6b-45b5-8dcc-eff0bbeda7ab",
    "VideoIndexerApiKey": "7c20ef7dee2e4235b6897bf066526a52"
  }
}
```
## Cloud Deployment
### Deploying to Azure
* Press "F1" and select Deploy to function app
* From there, follow the instructions to deploy your own app in azure subscription.
### Azure Hosted Application Configuration
When the application is deployed, you can go configure your deployed application settings. This can be done in many ways. Two known ways is through the [Azure Portal](https://docs.microsoft.com/en-us/azure/app-service/configure-common) and directly [From VS Code](https://docs.microsoft.com/en-us/azure/azure-functions/functions-develop-vs-code?tabs=csharp#publish-application-settings).
Once the application configurations has changed, you have to restart the application for the changes to take effect.

## Consuming the files
This application trigger file conversion only when a blob file with a specific path and format is created into the configured storage account. The chunk conversion will succeed only if there is a related init file already stored in the container. 
**Container Name** : "camera1"
**Chunk Blob File Path** : "{timestamp}/chunk-stream_{track}-{chunkId}.m4s"
**Related Init File Path** : "{timestamp}/init-stream_{track}.m4s"