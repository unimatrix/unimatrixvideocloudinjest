using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;

namespace Unimatrix.Video
{
    public static class ConvertM4sToMp4BlobTrigger
    {
        [FunctionName("ConvertM4sToMp4BlobTrigger")]
        public static async Task Run(
            [BlobTrigger("camera1/{timestamp}/chunk-stream_{track}-{chunkId}.m4s", Connection = "AzureWebJobsStorage")] Stream chunkBlob,
            [Blob("camera1/{timestamp}/init-stream_{track}.m4s", FileAccess.Read)] Stream initStreamBlob,
            [Blob("camera1/{timestamp}/chunk-stream_{track}-{chunkId}.mp4")] ICloudBlob mp4ChunkBlob,
            string track,
            string chunkId,
            ILogger log)
        {
            using (var outputStream = new MemoryStream())
            {
                log.LogInformation("Building MP4 for chunk-stream_{track}-{chunkId}.m4s ({chunkBlobLength}KB)", track, chunkId, chunkBlob.Length / 1024);
                log.LogInformation("Associated init stream : init-stream_{track}.m4s ({initBlobLength}KB)", track, initStreamBlob.Length / 1024);

                await initStreamBlob.CopyToAsync(outputStream);
                await chunkBlob.CopyToAsync(outputStream);

                log.LogInformation("Producing MP4 : chunk-stream_{track}-{chunkId}.mp4 ({mp4BlobLength}KB)", track, chunkId, outputStream.Length / 1024);
                outputStream.Seek(0, 0);
                await mp4ChunkBlob.UploadFromStreamAsync(outputStream);
                log.LogInformation("chunk-stream_{track}-{chunkId}.mp4 is transfered", track, chunkId);
            }
        }
    }
}
