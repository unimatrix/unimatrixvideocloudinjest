using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace Unimatrix.Video
{
    public class FunctionConfiguration
    {
        private IConfigurationRoot _configurationBuilder;
        
        public string this[string name]
        {
            get { return _configurationBuilder[name]; }
            set { _configurationBuilder[name] = value; }
        }
        public FunctionConfiguration(ExecutionContext context)
        {
            _configurationBuilder = new ConfigurationBuilder()
				.SetBasePath(context.FunctionAppDirectory)
				.AddJsonFile("local.settings.json", optional: true, reloadOnChange: true) // <- This gives you access to your application settings in your local development environment
				.AddEnvironmentVariables() // <- This is what actually gets you the application settings in Azure
				.Build();
        }
    }
}