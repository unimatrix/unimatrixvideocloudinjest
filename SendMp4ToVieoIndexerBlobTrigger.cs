using System;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;

namespace Unimatrix.Video
{
    public static class SendMp4ToVieoIndexerBlobTrigger
    {
        [FunctionName("SendMp4ToVieoIndexerBlobTrigger")]
        public static async Task Run(
            [BlobTrigger("camera1/{timestamp}/chunk-stream_{track}-{chunkId}.mp4", Connection = "AzureWebJobsStorage")]Stream myBlob, 
            string track,
            string chunkId,
            string timestamp,
            ILogger log,
            ExecutionContext context)
        {
            var functionConfiguration = new VideoCloudInjestConfiguration(context);

            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.AllowAutoRedirect = false;
            using(var httpClient = new HttpClient(httpClientHandler))
            {
                httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", functionConfiguration.VideoIndexerApiKey);

                var accountAccessTokenRequestResult = await httpClient.GetAsync($"{functionConfiguration.VideoIndexerApiUrl}/auth/{functionConfiguration.VideoIndexerLocation}/Accounts/{functionConfiguration.VideoIndexerAccountId}/AccessToken?allowEdit=true");
                var accountAccessToken = accountAccessTokenRequestResult.Content.ReadAsStringAsync().Result.Replace("\"", "");

                log.LogInformation("camera1/{timestamp}/chunk-stream_{track}-{chunkId}.mp4 : Got access token to upload video",timestamp, track, chunkId);

                httpClient.DefaultRequestHeaders.Remove("Ocp-Apim-Subscription-Key");

                using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    content.Add(new StreamContent(myBlob),"bilddatei",$"newchunk-stream_{track}-{chunkId}.mp4");
                    log.LogInformation("Start uploading {timestamp}/chunk-stream_{track}-{chunkId}.mp4 to Azure Video Indexer",timestamp, track, chunkId);
                    using (var message = await httpClient.PostAsync($"{functionConfiguration.VideoIndexerApiUrl}/{functionConfiguration.VideoIndexerLocation}/Accounts/{functionConfiguration.VideoIndexerAccountId}/Videos?accessToken={accountAccessToken}&name={timestamp}-chunk-stream_{track}-{chunkId}&description=Fredrik neutral position&privacy=private&partition=some_partition", content))
                    {
                        log.LogInformation("Upload for {timestamp}/chunk-stream_{track}-{chunkId}.mp4 to Azure Video Indexer is completed",timestamp, track, chunkId);
                        var response = await message.Content.ReadAsStringAsync();
                        log.LogInformation("camera1/{timestamp}/chunk-stream_{track}-{chunkId}.mp4 : Upload result : {response}",timestamp, track, chunkId, response);
                    }                    
                }
            }
        }
    }
}
