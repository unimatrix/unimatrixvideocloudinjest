using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace Unimatrix.Video
{
    public class VideoCloudInjestConfiguration
    {
        private FunctionConfiguration _applicationConfiguration;

        public string VideoIndexerApiUrl{get{return _applicationConfiguration["VideoIndexerApiUrl"];}}
        public string VideoIndexerLocation{get{return _applicationConfiguration["VideoIndexerLocation"];}}
        public string VideoIndexerAccountId{get{return _applicationConfiguration["VideoIndexerAccountId"];}}
        public string VideoIndexerApiKey{get{return _applicationConfiguration["VideoIndexerApiKey"];}}
        
        public VideoCloudInjestConfiguration(ExecutionContext context)
        {
            _applicationConfiguration = new FunctionConfiguration(context);
        }
    }
}